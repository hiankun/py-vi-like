[Fri Sep 15 09:34:01 AM CST 2023]

* Searched for `how to create vim like editor` and found this:
  https://stackoverflow.com/q/10902630
* The original source `source-archive.zip` was downloaded from:
  https://code.google.com/archive/p/pyvi/source/default/source
* Convert to Python3 version.
  * Modified `raise` string format to f-string.
  * Follow https://stackoverflow.com/a/27030934 to resolve the following error:
    ```
    ModuleNotFoundError: No module named 'exceptions'
    ```
  * Modified `has_key()` to `in`.
* In 2023 there's another `pyvi` package, so rename this to `pyvilike`.
* The code can be executed by `python pyvi.py`, then you can press `o` to type.
  (Press `i` and try to type will cause error and I don't know why.)
  But I have not figured out how to do other things yet...
